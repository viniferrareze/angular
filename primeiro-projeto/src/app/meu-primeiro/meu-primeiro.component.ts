import {Component} from '@angular/core'; //Importa o modulo no angular da classe Component


//CRIA A CLASSE 
@Component({
    selector: 'meu-primeiro-component', //Nome da tag HTML...
    template: `
       <p>Meu primeiro componente com Angular2!</p>
    `
}) //Anotação para dizer que é um componente do Angular2

export class MeuPrimeiroComponent{} //Cria a classe MeuPrimeiroComponente