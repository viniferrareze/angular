import { BrowserModule } from '@angular/platform-browser'; //Prepara a aplicação para ser executada em um Browser
import { NgModule } from '@angular/core'; 
import { FormsModule} from '@angular/forms';
import { HttpModule} from '@angular/http'; //Trabalha com requisições AJAX ou seja GET, SET, POST

//Imports da nossa classe
import { AppComponent } from './app.component';
import { MeuPrimeiroComponent } from './meu-primeiro/meu-primeiro.component';
import { MeuPrimeiro2Component } from './meu-primeiro2/meu-primeiro2.component' //faz referença ao componente
import { CursosModule } from './cursos/cursos.module';

@NgModule({  //Decorar essa classe vai representar um MODULO

  declarations: [ //Listar todos os componentes, diretivas e paypers para utilizar neste modulo
    AppComponent,
    MeuPrimeiroComponent,
    MeuPrimeiro2Component //declara q vai utilizar o componente novo criado
  ],

  imports: [ //Outros modulos que pode ser utilizado dentro desse modulo, ou dentro dos declarados no DECLARATIONS;
    BrowserModule,
    FormsModule,
    HttpModule,
    CursosModule
  ],


  providers: [], //Onde vai ser colocado o serviço(Verificação, login, guarda de rotas, ecobo global) disponivel para todos os componentes
  bootstrap: [AppComponent] //Componente deve ser instanciado, o componente Componente principal do projeto...
})

export class AppModule { } //Exporta o modulo RAIZ
