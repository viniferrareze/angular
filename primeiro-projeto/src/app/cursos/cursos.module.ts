import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  //Diferenca do modulo RAIZ para o modulo de FUNCIONALIDADES

import { CursosComponent } from './cursos.component';
import { CursoDetalheComponent } from './curso-detalhe/curso-detalhe.component';
import { CursosService } from './cursos.service';

@NgModule({
  imports: [
    CommonModule
  ],

  
  declarations: [
    CursosComponent,
    CursoDetalheComponent //componente privado que so pertence dentro do componente 
  ],

  exports: [  //Exporta as declarações desse modulo que pode ser exposto para outros nomes;
    CursosComponent
  ],

  providers: [ //Fornecedor, quais são os servicos fornecedores 
    CursosService
  ]
})
export class CursosModule { }
