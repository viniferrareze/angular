import { Injectable } from '@angular/core';

@Injectable() //Seguinifica que esta classe pode ser enjetada em outra classe... 
export class CursosService {

  constructor() { 

  }

  public getCursos(){
     return ['Java', 'Angular', 'JS'];
  }

}
