import { Component, OnInit } from '@angular/core';

import { CursosService } from './cursos.service';


@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})


export class CursosComponent implements OnInit {
  
  public nomePortal : string;  
  public cursos : string[];


  constructor(private CursosService : CursosService) { // é a classe instanciada
     this.nomePortal = 'MEU PRIMEIRO PORTAL!'; 
     
     this.cursos = this.CursosService.getCursos();    
  }

  ngOnInit() {
  }

}
