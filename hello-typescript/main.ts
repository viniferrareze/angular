var minhaVar = 'minha variavel'; //em JS

function minhaFunc(x,y){
   return x + y;    
}

//ES 6 ou ES 2015
let num = 2;      //Denife a varivel
const pi = 3.14;  //Denife a variavel


//Array de function em JS
var numeros = [1,2,3];
numeros.map(function(valor){
    return valor * 2;
});

//Array de function em TS => mesma coisa que declarar a função. no exemplo de cima.
numeros.map( valor =>{valor * 2}); //ES 2015

//ES JS PURO definição de classe. 
class Matematica{
   soma(x,y){
       return x + y;
   }
}

// no type script é possivel tipar o tipo da variavel. 
let n1 : String; 


