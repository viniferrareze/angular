import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-curso',
  templateUrl: './input-property.component.html',
  styleUrls: ['./input-property.component.css']
})
export class InputPropertyComponent implements OnInit {
  @Input('nome') nomeCurso : string = ''; //É utilizando o decoreytor Input, 
  // que faz parte do pacote angularCore. com isso é exposto uma 
  //propriedade para ser utilizado em outro componente. internamente é nomeCurso, e o nome exposto é 'nome';

  constructor() { }

  ngOnInit() {
  }

}
