import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'contador',
  templateUrl: './output-property.component.html',
  styleUrls: ['./output-property.component.css']
})
export class OutputPropertyComponent implements OnInit {
  @Input() valor : number = 0; 

  @Output() mudouValor = new EventEmitter(); //expor um evento 

  @ViewChild('campoInput') campoValorInput : ElementRef; //Associação da variavel do tamplente com o a variavel cdo tamplete

  constructor() { }


  public incrementa(){
     //this.valor++;  
     this.campoValorInput.nativeElement.value++;
     this.mudouValor.emit({novoValor : this.valor});
  }

  public decrementa(){
    this.campoValorInput.nativeElement.value--;
    this.mudouValor.emit({novoValor : this.valor});  
 }

  ngOnInit() {
  }

}
