import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
  //styles : [AQUI È UM ARRAY DE CSS]
})


export class DataBindingComponent implements OnInit {
  public url : string = 'http://www.google.com.br'; 
  public cursoAngular : boolean = true;
  public urlImagem : string = 'http://lorempixel.com/400/200/sports/';
  public valorAtual : string = '';
  public valorEnter : string = '';
  isMouseOver : boolean = false;
  public nome : string = 'abc';

  public pessoa : any = {
     nome : 'Exemplo',
     idade : 10   
  }

  public nomeDoCurso : string = 'Curso de angular';

  constructor() { }


  public getValor(){
     return 22;   
  }

  public botaoCliclado(){
     alert("tesste");  
  }

  public onKeyUp(evento: KeyboardEvent){

     this.valorAtual = (<HTMLInputElement>evento.target).value;

  }

  public salvarValor(valor){
    this.valorEnter = valor;
  }


  public onMouseOver(){
     this.isMouseOver = !this.isMouseOver;
  }


  public onMudouValor(evento){
    console.log(evento.valor);
  }

  ngOnInit() {
  }

}
