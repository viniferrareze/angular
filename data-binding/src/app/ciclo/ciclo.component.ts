import { Component, OnInit, OnChanges, DoCheck, AfterContentChecked, AfterViewInit, OnDestroy, Input } from '@angular/core';

@Component({
  selector: 'app-ciclo',
  templateUrl: './ciclo.component.html',
  styleUrls: ['./ciclo.component.css']
})
export class CicloComponent implements OnInit, OnChanges, DoCheck, AfterContentChecked, AfterViewInit, OnDestroy   {
  
  @Input() valorIniciar : number = 0;
  
  constructor() { 
     this.log('chamou evento constructor');
  }

  ngOnChanges() {
    this.log('chamou evento ngOnChanges');
  }

  ngOnInit() {
    this.log('chamou evento ngOnInit');
  }

  ngDoCheck() {
    this.log('chamou evento ngDoCheck');
  }

  ngAfterContentChecked() {
    this.log('chamou evento ngAfterContentChecke');
  }

  ngAfterViewInit() {
    this.log('chamou evento ngAfterViewInit');
  }

  ngOnDestroy() {
    this.log('chamou evento ngOnDestroy');
  }



  private log(name : string){
     console.log(name);
  }
}
