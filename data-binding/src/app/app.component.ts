import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public valor : number = 23; 
  public deletarCiclo : boolean = false;

  public mudarValor(){
    this.valor++;
  }

  public destruirCiclo(){
    this.deletarCiclo = true;
  }
}
