import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-operador-elvis',
  templateUrl: './operador-elvis.component.html',
  styleUrls: ['./operador-elvis.component.scss']
})
export class OperadorElvisComponent implements OnInit {

  public tarefa : any = {
     desc: 'Descrição da tarefa',
     responsavel: null  
     //resposvel : {
     //    nome : 'teste'
     //}
  }

  constructor() { }

  ngOnInit() {
  }

}
