import { Directive, HostListener, HostBinding, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[HighlightMouse]'
})
export class HighlightMouseDirective {
  @HostListener('mouseenter') onMouseEnter(){  //Lista de evento. Quero escutar o evento mouse enter, entao vai fazer alguma coisa
    /* this._renderer.setElementStyle(this._elementRef.nativeElement,
                                    'background-color',
                                    'yellow');*/
    this.backgroundColor = 'yellow';                                
    
  }

  @HostListener('mouseleave') onMouseLeave(){  //Lista de evento. Quero escutar o evento mouse enter, entao vai fazer alguma coisa
   /* this._renderer.setElementStyle(this._elementRef.nativeElement,
                                   'background-color',
                                   'white');*/
    this.backgroundColor = 'white';  

 }

 //@HostBinding('style.backgroundColor') backgroundColor : string; //Permite que seja feita a associção de um atributo ou classe do HTML para um variavel

 @HostBinding('style.backgroundColor') get setColor(){
   //Codigo Extra;
   return this.backgroundColor;
 }

 private backgroundColor : string;

 constructor(/*private _elementRef : ElementRef, private _renderer:Renderer*/) { 

 }

}
