import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: 'p[FundoAmarelo]' //Se eu quero q seja aplicado a qlqr tipo do componente, deve ser colocodao o nome da tag nos []
})
export class FundoAmareloDirective {

  constructor(private _elementRef: ElementRef, private _renderer : Renderer) { 
     //console.log(this._elementRef);  //Elemento do DOM NUNCA USAR ESSE Element usar o Renderer
     //Por motivo de proteger contra ataques em sua aplicação

     //this._elementRef.nativeElement.style.backgroundColor = 'yellow';
     this._renderer.setElementStyle(this._elementRef.nativeElement, 
                                   'background-color', 
                                   'yellow' );

  }

}
