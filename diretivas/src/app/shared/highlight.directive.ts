import { Directive, HostListener,  HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[Highlight]'
})
export class HighlightDirective {

  @HostListener('mouseenter') onMouseEnter(){  //Lista de evento. Quero escutar o evento mouse enter, entao vai fazer alguma coisa
    this.backgroundColor = this.highlightColor;                                   
  }

  @HostListener('mouseleave') onMouseLeave(){  //Lista de evento. Quero escutar o evento mouse enter, entao vai fazer alguma coisa
    this.backgroundColor = this.defaultColor;  
 }


 @HostBinding('style.backgroundColor') get setColor(){
   return this.backgroundColor;
 }


 @Input() defaultColor : string = 'white'
 @Input('Highlight') highlightColor : string = 'yellow'

 private backgroundColor : string;

 constructor() { 

 }

 ngOnInit(){
    this.backgroundColor = this.defaultColor;  
 }

}
